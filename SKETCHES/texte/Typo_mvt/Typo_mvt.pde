
// On declare une variable de type PFont . une classe.
PFont font;

// Une variable s, qui contiendra le mot FREE
String s = "FREE";
float angle = 0.0;

void setup() {
  size(150,150);
  
  // après avoir crée une font avec le menu Tools->creat font, on la charge
  font = loadFont("Dimnah-48.vlw");
  
  // on la défini comme la font à utiliser
  textFont(font);
  fill(0, 175);
}
void draw() {
  background(255);
  
  // on incrémente la variable angle à chaque rafraichissement.
  angle += 0.08;
  
  // la boucle qui  permet de parcourir le String
  for(int i = 0; i < s.length(); i++) {

    // une variable pour générer le mouvement.
    float c = sin(angle + i/PI);
    
    // La taille des caractères influencée par c
    textSize((c + 1.0) * 32);
    
    // on affiche le texte, caractère par caractère.
    text(s.charAt(i), 20 + i*28, 100);
  }
}

