// une variable de type PFont : une classe.
PFont f;

// 2 variables primitives de type char
char o = 'A';
char m = 'Z';


void setup() {
  size(300, 200);

  // on charge la font
  f = loadFont("DejaVuSans-ExtraLight-48.vlw");

  // on défini la font pour le texte
  textFont(f);
  
  // on défini la couleur pour la font
  fill(0, 35);
}

void draw() {

  // 2 variables pour stocker la position de la souris
  float x = mouseX; 
  float y = mouseY; 

  // si je suis en deça de la moitié de l'écran ...
  if(x < width/2) { 
    text(o, x, y);
  }
  else {
    text(m, x, y);
  }
  
  line(width/2, 0, width/2, height);
}

