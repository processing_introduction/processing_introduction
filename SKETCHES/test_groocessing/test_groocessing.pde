int taille = 15;
void setup() {
  size(300,200);
}
void draw() {
  stroke(30,120,0);
  noFill();
  background(255);
  for(float i=0; i<width; i+=taille+2) {
    for(float j=0; j<height; j+=taille+2) {
//  fill(100, (int)random(255), 100);  
      float twist = random(height);
      twist = norm(twist, 0, height);
      twist = pow(twist, 0.5);
      stroke(255,0,0);
      rect(i,j+twist,taille,taille);
    }
  }
}

