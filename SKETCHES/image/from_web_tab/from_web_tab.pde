
// une variable pour le nombre total d'images
int nb = 3;

// un tableau contenant 3 images
PImage m [] = new PImage[3];

// idem pour les String
String [] url = new String[3];

// un tableau de String, que l'on défini directement. il contient les noms des images
String [] name = {
  "Aou1", "Update2", "creationSonore"
};

void setup() {
  
  // la boucle permettant de charger les images en mémoire
  for(int i = 0; i < nb; i++) {
    url[i] = "http://madosedesoma.free.fr/public/photos/"+ name[i]+".jpg";
    m[i] = loadImage(url[i]);
  }
  
  // on défini une variable qui stocke la hauteur des images.
  int h = m[0].height;
  
  // on défini la taille de la fenetre en fonction de cette taille.
  size(m[0].width, h * 3);
  
  // une boucle pour afficher les images.
  for(int i = 0; i < nb; i++) {
    image(m[i], 0, (i*h)-1 );
  }
  
}

