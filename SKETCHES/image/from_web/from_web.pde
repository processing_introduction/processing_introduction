
// Je déclare 3 variables de types PImage
// PImage est un type de variable complexe (une classe), on la reconnait à la majuscule.
PImage m, n , p;

// Je déclare et défini 3 variables de type String, chaine de caractères
// Les adresses url des images.
String url_1 = "http://madosedesoma.free.fr/public/photos/Aou1.jpg";
String url_2 = "http://madosedesoma.free.fr/public/photos/Update2.jpg";
String url_3 = "http://madosedesoma.free.fr/public/photos/creationSonore.jpg";

void setup() {
  // je charge en mémoire les images
  m = loadImage(url_1);
  n = loadImage(url_2);
  p = loadImage(url_3);
  
  // une variable pour la hauteur de chaque image (toutes identiques)
  int h = m.height;

  // la taille de ma fenetre en fonction de cette variable
  size(m.width, h * 3);
  
  // j'affiche mes images
  image(m, 0, 0);
  image(n, 0, h-1);
  image(p, 0, (h*2)-1);
}

