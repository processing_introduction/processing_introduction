// On déclare une variable de type PImage. une class.
PImage keaton;

void setup() {
  size(240, 320);
  noStroke();
  
  // On charge l'image en mémoire
  keaton = loadImage("keaton.jpg");
  
  // On redimensionne l'image
  keaton.resize(240, 320);
} 
void draw() {
  
  // Si une touche du clavier est pressée
  if(keyPressed) {
    // on défini x qui limite la position de la souris en X, entre 0 et width/2
    int x = constrain(mouseX, 0, width/2);
    
    // on affiche l'image à cet endroit avec la fonction set()
    set(x, 0, keaton);
  }
  else {
    // Sinon on affiche l'image 
    image(keaton, 0, 0);
    
    // et on récupère la valeur colorimétrique du pixel à l'emplacement de la souris
    color c = get(mouseX, mouseY);
    
    // cette valeur nous sert à peindre l'interieur d'un rectangle
    fill(c);
    rect(width/2, 0, width/2, height);
  }
}

