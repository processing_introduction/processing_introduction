PImage img;
boolean glitch = false;
int compteur = 0;

void setup() {
  size(300, 200);
  background(255);
}

void draw() {
  int s = (int)random(6);
  int x = mouseX + s;
  int y = mouseY + s;
  int count = width * height;

  if(keyPressed) {
    if(key == 'a' || key == 'A') {
      glitch = true;
    }
    if(key == 'z' || key == 'Z') {
      glitch = false;
      compteur = 0;
    }
  }

  if(glitch) {
    img = get();
    img.loadPixels();
    loadPixels();
    for(int i = 0; i < count; i++) {
      pixels[i] = img.pixels[i/2];
    }
    if(compteur == 0) updatePixels();
    compteur++;
  }
  else {
    loadPixels();
    for(int i = 0; i < 18; i++) {
      for(int j = 0; j < 18; j++) {
        color c = color((int)random(255), (int)random(155), (int)random(155, 255), 10);
        int _x = x+i;
        int _y = y+j;
        int mx = constrain(_x, 0, width);
        int my = constrain(_y, 0, height);
        if((my*width + mx) < count) pixels[my*width + mx] = color(c);
      }
    }
    updatePixels();
  }
  println(glitch);
}

