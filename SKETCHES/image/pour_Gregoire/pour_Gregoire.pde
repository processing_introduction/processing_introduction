PFont f;
int tirage, timer;

// variables dans lesquelles on mémorise les changements.
int [][] change;
int [][] choix;

// mon tableau de lettres, pour remplacer ton tableau de photos
char[] lettres = {
  'a', 'j', 'k', 'l', 'm', 'o', 'p'
};

void setup() {
  size(200,200);

  String[] fontList = PFont.list();
  f = createFont(fontList[15], 10);
  textFont(f);

  // on défini les doubles tableaux
  change = new int[width][height];
  choix = new int[width][height];

  tirage = (int)random (20, 30);
}

void draw() {

  background (183,22,162);
  stroke (255,0,200);

  // on vérifie si la variable d'incrémentation est inférieure à 5 (à changer en fonction du CPU, envies...)
  if(timer<5)timer++;
  else timer=0;

  // si le timer "boucle" et repart de zéro, on lance un update()
  if(timer==0)update();
  
  // on affiche toujours l'image à l'écran
  display();

//  println(timer);
}

void update() {
  // on tire des valeurs au hasard pour toutes les photos et rect.
  for(int i=0; i<width; i++) {
    for(int j=0; j<height; j++) {
      change[i][j] = (int)random(5, 10);
      choix[i][j] = (int)random (lettres.length);
    }
  }
}

void display() {
  // on affiche en permanence les rect et images avec les variables 'change' et 'choix' updatées
  for(int i=0; i<width; i++) {
    for(int j=0; j<height; j++) {
      color c = color(255,0,200,100);
      fill (c);

      rect(i*tirage, j*tirage, change[i][j], change[i][j]);

      fill(0,50);

      text(lettres[choix[i][j]], i*tirage, j*tirage);
    }
  }
}

