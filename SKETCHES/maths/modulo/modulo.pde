// Rappel :
// Modulo : type de division dont seul le reste est considéré
// exemple : 10 mod 3 = 1 (10 divisé par 3 égale 3, reste 1)
// exemple : 26 mod 12 = 2
// modulo se note % :
// 17 % 5 = 2

float a = 0.0;

void setup(){
  smooth();
}

void draw(){

  a = (a + 0.5)%width;
  stroke(0,10);
  line(a, 0, a, height);

}


