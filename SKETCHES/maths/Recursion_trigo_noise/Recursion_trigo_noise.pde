float x, y, rayon=1.0, taille = 0.2, v = 0.0, inc = 0.1, pas = 6f;
int nbPoints = 360;

void setup() {
  size(400, 300);
  background(255);
  smooth();
  noiseSeed(0);
  drawCircle(5, pas);
}

void drawCircle(int nb, float pas) {
  rayon = 1.0;

  for(float i=0; i<nbPoints*5 ;i+=pas) {
    pushMatrix();
    float changeColor =  noise(v)*10;
    float angle = radians(i);
    float alphX = sin(angle);
    float alphY = cos(angle);
    alphX += PI/6.0;
    x = width/2 +(alphX * rayon);
    y = height/2 + (alphY * rayon);
    v+=inc;
    println(changeColor);
    strokeWeight(1);
    if(changeColor>5) {
      changeColor = map(changeColor,5,9,9,5);
      stroke(changeColor*25);
      fill(changeColor*25);
    }
    else { 
      noFill();
      stroke(0,90);
    }
    translate(x-35, y);

    ellipse(0, 0, taille, taille);
    rayon += 0.33;
    taille += 0.03;
    popMatrix();
  }
}

