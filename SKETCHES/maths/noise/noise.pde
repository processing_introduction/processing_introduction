//-------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------  Formation Labomedia Octobre 2010
//-----------------------------------------------------------------------------------------  Thomas bernardi Copyleft
//------------------------------------------------------------------------------------------------------------- Lolab
//----------------------------------------------------------------------------------inspiré d'un code d'Ira Greenberg

int rayon = 5 ;      
int incrementation=0;
float noiseScale = 0;//--------------------------- valeur de noise attribuée aus abscisses
float noiseScaleY= 0;//--------------------------- valeur de noise attribuée aus ordonnées
float x, y;

void setup(){
  size(600,200);    
  background(125);  
  noStroke();       
  smooth();         
  frameRate(20);    
  rectMode(CENTER);
}

void draw(){

  fill(125,1);
  noStroke();
  rect(0,0,width*2, height*2);
  float transparence = 0; 
  //----------------------                                       
  if(incrementation<width){
    noiseScale+=.02;//-------------------------------- on incrémente la valeur.
    noiseScaleY+=.025;
    //---------------------------------
    float noiseVal = noise(noiseScale) ;     
    float noiseValY= noise(noiseScaleY);
    //----------------------------------
    transparence = noiseVal*75;             
    //---------------------------------- conversion    
    rayon = int(random(5,24));               
    x = noiseVal * width;
    y = noiseValY * height;
    //----------------------------------
    fill(255, transparence);   
    ellipse(x, y ,rayon, rayon);       
    //---------------------------------
    incrementation++;                  
  }
  //-------------------------------------
  incrementation=0;
}




