float x, y;
int rayon = 100;
int nbPoints = 360;
float pas = 5f;

void setup() {
  size(400, 300);
  background(255);
  noFill();
  smooth();
  drawCircle(5, rayon, pas);
}

void drawCircle(int nb, int rayon, float pas) {
  for(float i=0; i<nbPoints ;i+=pas) {
    float angle = radians(i);
    float alphX = sin(angle);
    float alphY = cos(angle);
    alphX += PI/6.0;
    x = width/2 +(alphX * rayon);
    y = height/2 + (alphY * rayon);
    strokeWeight(1);
    stroke(0,90);
    ellipse(x, y, 5, 5);
  }    
  if(nb>0) {
    nb--;
    rayon -= 17;
    pas *= 1.3;
    drawCircle(nb, rayon, pas);
  }
}

