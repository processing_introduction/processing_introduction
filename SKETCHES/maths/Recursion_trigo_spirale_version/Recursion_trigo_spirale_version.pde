float x, y, rayon=1.0, taille = 0.2;
int nbPoints = 360;
float pas = 6f;

void setup() {
  size(400, 300);
  background(255);
  smooth();
  drawCircle(5, pas);
}

void drawCircle(int nb, float pas) {
  rayon = 1.0;
  for(float i=0; i<nbPoints*5 ;i+=pas) {
    float angle = radians(i);
    float alphX = sin(angle);
    float alphY = cos(angle);
    alphX += PI/6.0;
    x = width/2 +(alphX * rayon);
    y = height/2 + (alphY * rayon);
    strokeWeight(1);
    noFill();
    stroke(0,90);
    ellipse(x, y, taille, taille);
    rayon += 0.33;
    taille += 0.02;
  }
}

