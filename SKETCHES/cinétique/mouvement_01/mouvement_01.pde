float x, y, speedX, speedY, acceleration;

void setup() {
  size(400, 300);
  smooth();

  x = random(width);
  y = random(height);
  speedX = 0.8;
  speedY = 0.9;
}
void draw() {
  noStroke();
  fill(255,25);
  acceleration = random(-0.5, 0.5);
  rect(0,0,width, height);
  speedX += acceleration;
  speedY += acceleration;
  x +=speedX;
  y +=speedY;
  if(x<0 || x>width)speedX=-speedX;
  if(y<0 || y>height)speedY=-speedY;
  stroke(0);
  pushMatrix();
  translate(x, y);
//  scale(acceleration*2);
  ellipse(0, 0, 10, 10);
  popMatrix();
  println(speedX+" "+speedY);
}

