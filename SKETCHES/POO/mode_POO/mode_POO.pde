//-------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------  Formation Labomedia Octobre 2010
//-----------------------------------------------------------------------------------------  Thomas bernardi Copyleft
//------------------------------------------------------------------------------------------------------------- Lolab
//-------------------------------------------------------------------------------------------------------------------
// ------------------ Mode OOP ----------------------

// Je crée une class : un nouveau type de donnée
Dessin premier;
Dessin deuxieme;
Dessin troisieme;
AvecCouleur ouah; //------------------------------------- pour la classe qui hérite
float mx, my; 

void setup(){
  size(300,300);
  smooth();
  noCursor();

  //------------------------------------------------------- j'initialise mon objet "premier" en lui indiquant que la valeur utilisée sera 10.
  premier = new Dessin(10);
  deuxieme = new Dessin(13);
  troisieme = new Dessin(34);
  ouah = new AvecCouleur(7); //---------------------------pour la class qui hérite
}



void draw(){
  mx = mouseX;
  my = mouseY;
  background(125);
  premier.affiche(mx+7, my);  
  deuxieme.affiche(mx-7, my);
  troisieme.affiche(mx,my+3);
  ouah.affiche(mx, my+7); //-------------------------------- pour la classe qui hérite
}


// --------------- la class Dessin
class Dessin{
  //---------------------------------------------------------- les variables utilisées par chaque instance de la class
  int taille;
  int transparence = 125;
  //---------------------------------------------------------- le constructeur
  //un constructeur n'est rien d'autre qu'une méthode, sans valeur de retour, portant le meme nom que la classe.
  //Il peut disposer d'un nombre quelconque d'arguments (éventuellement aucun).
  Dessin(int itaille){
    taille = itaille;
  }

  void affiche(float x, float y){
    stroke(0);
    fill(255, transparence);
    ellipse(x, y, taille, taille);

  }
}

// *******************************
// je rajoute une class qui hérite des fonctionnalité de la classe parent "Dessin"

class AvecCouleur extends Dessin
{
  int R = 110;
  int V = 186;
  int B =  34;

  AvecCouleur(int taille){
    super(taille);

  }

  void affiche(float x, float y){
    //super.affiche(x,y);//---------------------- si je valide cette commande c'est la méthode de la superclass qui sera effectuée.
    noStroke();
    fill(R, V, B, super.transparence);
    ellipse(x, y, taille, taille);    
  }
}
