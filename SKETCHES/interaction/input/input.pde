float x, y;
int time;
boolean over = false;

void setup() {
  size(400, 300);
  smooth();
}

void draw() {
  background(255);
  noStroke();
  
  x = mouseX;
  y = mouseY; 
  
  time = (int)(millis()%frameRate);
  int c = (int)map(time, 0, 8, 155, 255);
  //println(time);

  if(mousePressed) {
    fill(c, 50);
    rect(0, 0, width, height);
  }

  if(x>20 && x<width-20 && y>20 && y<height-20)over=true;
  else over=false;

  for(int i=36; i<width-28; i+=10) {
    for(int j=36; j<height-28; j+=10) {
      float mouseDist = dist(x, y, i, j);
      float t = mouseDist/40;
      float angleX = map(x, 28, width-28, 0, TWO_PI);
      float angle = atan2((mouseY-j), (mouseX -i));
      pushMatrix();
      translate(i, j);
      rotate(angle);
      stroke(0);
      if(over)line(0, 0, t, t);
      popMatrix();
    }
  }
  noFill();
  rect(20, 20, width-40, height-40);
}

