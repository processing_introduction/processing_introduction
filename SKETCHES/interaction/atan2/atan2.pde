int nb = 3;

float x[] = new float [nb];
float y[] = new float [nb];

void setup() {
  size(400,300);
  smooth();
  strokeWeight(3);
  noFill();
  for(int i=0; i<nb; i++) {
    x[i] = random(width);
    y[i] = random(height);
  }
}
void draw() {
  background(255);
  for(int i=0; i<nb; i++) {
    
    // calcul la position en radians d'un point par rapport à l'origine
    float angle = atan2(mouseY-y[i], mouseX-x[i]);
    
    // on mémorise les transformation de matrice
    pushMatrix();
    
    // on déplace la matrice
    translate(x[i], y[i]);
    rotate(angle);
    stroke(0,175);
    triangle(-20,-8,20,0,-20,8);
    ellipse(-8,6,5,5);
    ellipse(-8,-6,5,5);
    popMatrix();
  }
}

