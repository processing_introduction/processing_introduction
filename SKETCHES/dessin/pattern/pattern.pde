float x, y=0;
int espace = 20, taille = 10;

size(300,200);
background(255);
rectMode(CENTER);
strokeWeight(7);
noFill();
//smooth();

for(int i=taille; i<width; i+=espace) {
  for(int j=taille; j<height; j+=espace) {
    float z = random(width);
    float p = random(height);
    float zN = norm(z, 0, width);
    float pN = norm(p, 0, height);
    x = pow(zN, 0.8);
    y = pow(pN, 0.4);
    x+=i;
    y+=j;
//    x*=width;
//    y*=height;
    color c = color((int)random(255), 0, (int)random(255));
    stroke(c);
    rect(x, y, taille, taille);
  }
}

